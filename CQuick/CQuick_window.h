﻿#pragma once
#undef  WINVER  
#undef _WIN32_WINNT
#define _WIN32_WINNT 0x500
#define WINVER   0x500 // Make it possible to use animatedwindow functions
#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>
#include <CommCtrl.h>
#include <AccCtrl.h>
#include <AclAPI.h>
#include <tchar.h>
#include <atlstr.h>
#include <comutil.h>
#include <iostream>
#include <thread>
#include <ShlObj.h>
#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "comsupp.lib")
namespace CQWindow {
	HWND SWAP_HWND;
#define PIE 3.1415926
#define	WM_TESTTASKMGR		0x40b
#define AN_SHOW				AW_ACTIVATE    // Active window
#define AN_HIDE				AW_HIDE        // Hide window
#define AN_FOLD				AW_CENTER      // Inside and outside folding
#define AN_LEFT				AW_HOR_POSITIVE// From left to right
#define AN_RIGTH			AW_HOR_NEGATIVE// From rigth to left
#define AN_SCROLL			AW_SLIDE       // Scroll animation
#define AN_DOWN				AW_VER_POSITIVE// From top to bottom
#define AN_UP				AW_VER_NEGATIVE// From bottom to top
extern "C" IMAGE_DOS_HEADER __ImageBase;
HINSTANCE hInstance = (HINSTANCE)&__ImageBase;
	class Window {
	static LRESULT __stdcall WindowProc(
		HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
		if (Window* ptr = reinterpret_cast<Window*>(GetWindowLongPtr(hWnd, GWLP_USERDATA)))
			return ptr->DoMessage(hWnd, message, wParam, lParam);
		else
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	LRESULT DoMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
		switch (msg) {
		case WM_DESTROY:
			PostQuitMessage(0);
			exit(0);
			return 0;
		case WM_CLOSE:
			 PostQuitMessage(0);
			 break;
		}
		this->Message = msg;
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
public:
	UINT Message = NULL;
	std::wstring LoadImageUrl;
	HWND hWnd;
	bool DoMessages() {
		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		if (msg.message == WM_QUIT) {
			return false;
		}
		return true;
	}
	Window(int x,int y,int px,int py,std::wstring titlel) {
		HINSTANCE hInstance;
		hInstance = GetModuleHandle(NULL);	
		WNDCLASS Render_WND;
		Render_WND.cbClsExtra = 0;
		Render_WND.cbWndExtra = 0;
		Render_WND.hCursor = LoadCursor(hInstance, IDC_ARROW);
		Render_WND.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
		Render_WND.lpszMenuName = NULL;	
		Render_WND.style = CS_VREDRAW;	
		Render_WND.hbrBackground = (HBRUSH)COLOR_WINDOW;	
		Render_WND.lpfnWndProc = WindowProc;	
		Render_WND.lpszClassName = _T("RenderWindow");	
		Render_WND.hInstance = hInstance;

		RegisterClass(&Render_WND);

		hWnd = CreateWindow(
			_T("RenderWindow"),           
			titlel.c_str(), 
			WS_OVERLAPPEDWINDOW, 
			px,             
			py,             
			x,               
			y,                
			NULL,               
			NULL,               
			hInstance,          
			NULL);              
		this->hWnd = hWnd;
		ShowWindow(hWnd, SW_SHOW); // Snap our window to the user's desktop res, minus taskbar etc.
		SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));
		SetWindowPos(hWnd, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED); // Make sure that our WindowLongPtr is updated.
		MoveWindow(hWnd, px, py, x, y, FALSE);
	}
};
	/*This class encapsulates window operations.
	It allows you to better operate your windows.
	You can customize his HWDN, or you can use
	the initialization (default binding to the window you have now).
	window w ("windows title name"); to find the window with this
	title name, this class just encapsulates some basic win32,
	suitable for newcomers*/

	class window {
	private:
		bool DoMessages() {
			MSG msg;
			while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			if (msg.message == WM_QUIT) {
				return false;
			}
			return true;
		}
		static LRESULT __stdcall WindowProc(
		HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
			if (window* ptr = reinterpret_cast<window*>(GetWindowLongPtr(hWnd, GWLP_USERDATA)))
				return ptr->DoMessage(hWnd, message, wParam, lParam);
			else
				return DefWindowProc(hWnd, message, wParam, lParam);
		}
		LRESULT DoMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
			switch (msg) {
			case WM_DESTROY:
				PostQuitMessage(0);
				exit(0);
				break;
			}
			this->WindowMessage = msg;
			return DefWindowProc(hWnd, msg, wParam, lParam);
		}
		std::wstring convert(const std::string& string) {
			_bstr_t t = string.c_str();
			wchar_t* swap_char = (wchar_t*)t;
			std::wstring return_ = swap_char;
			return return_;
		}
		std::string convert(LPCWSTR lps) {
			int len = WideCharToMultiByte(CP_ACP, 0, lps, -1, NULL, 0, NULL, NULL);
			if (len <= 0) {
				return "";
			}
			else {
				char* dest = new char[len];
				WideCharToMultiByte(CP_ACP, 0, lps, -1, dest, len, NULL, NULL);
				dest[len - 1] = 0;
				std::string str(dest);
				delete[] dest;
				return str;
			}
		}
		void NewWindow(int x, int y, int px, int py, std::wstring titlel) {
			HINSTANCE hInstance = GetModuleHandle(NULL);
			WNDCLASSEX wc;
			HWND hwnd;
			wc.cbSize = sizeof(WNDCLASSEX);
			wc.style = CS_HREDRAW | CS_VREDRAW;
			wc.lpfnWndProc = WindowProc;
			wc.cbClsExtra = wc.cbWndExtra = NULL;
			wc.hInstance = hInstance;
			wc.hbrBackground = (HBRUSH)GetStockObject(COLOR_WINDOW + 1);
			wc.lpszMenuName = NULL;
			wc.lpszClassName = titlel.c_str();
			wc.hIcon = wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
			wc.hCursor = LoadCursor(NULL, IDC_ARROW);
			RegisterClassEx(&wc);
			hwnd = CreateWindowEx(NULL,
				wc.lpszClassName,
				wc.lpszClassName,
				WS_OVERLAPPEDWINDOW,
				CW_USEDEFAULT,
				CW_USEDEFAULT,
				CW_USEDEFAULT,
				CW_USEDEFAULT,
				NULL,
				NULL,
				hInstance,
				NULL);
			ShowWindow(hwnd, SW_SHOW);
			UpdateWindow(hwnd);
			for (int i = 1; i <= 8; i++) {
				this->HWnd = hwnd;
				ShowWindow(this->HWnd, SW_SHOW);
				SetWindowLongPtr(this->HWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));
				SetWindowPos(this->HWnd, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED);
				MoveWindow(this->HWnd, px, py, x, y, FALSE);
				Sleep(10);
			}
		}
		HWND HWnd = 0;
		int  x	  = 0;
		int  y	  = 0;
		int  px	  = 0;
		int  py	  = 0;
		std::thread WinPath;
	public:
		UINT WindowMessage = NULL;
		LPCSTR title = "Window";
		void HIDE_ICON();
		void NO_COLSE_ICON();
		window();
		window(std::string);
		void SetCursorIcon(LPCWSTR);
		window(HWND);
		void Transparent(int);
		void Transparent(COLORREF);
		void EXIT(int);
		void DeleteTitleBox();
		BOOL SetHWND(std::wstring);
		void move(int, int);
		BOOL SetTitle();
		void SetHWnd(HWND);
		void RounWindow(int, int, int, int);
		void Flash(bool);
		void move_to(int, int);
		void MINI();
		BOOL Animate(DWORD, DWORD);
		BOOL is_in(HWND);
		HWND GetHWnd();
		void REDUCTION();
		void SHOWCMD();
		void LoadWindow();
		void GetHWndForMouse();
		HWND GetHWndForMouseAndReturn();
		void MAX();
		void SetIcon(std::wstring);
		void CQucikWindowThread();
		void line(int x, int y);
		void flicker();
		void NO_MINI();
		std::string __get_path__();
		void move_to(HWND);
		void PaintAgain();
		void HIDE();
		void FadeIn();
		void HIDECMD();
		void FadeOut();
		void SHOW();
		std::string GetFilePath();
		void NOSIZE();
		void draw_circel(int, int, int, int);
		void mouse_is_cheak_title_box();
		void closegraph();
		void initgraph(int,int,int,int);
		void initgraph(int,int);
		void TOP();
	};

	void window::LoadWindow() {
		Sleep(80);
	}
	void window::SHOWCMD() {
		HWND HWnd = GetConsoleWindow();
		ShowWindow(HWnd, SW_SHOW);
	}
	std::string window::GetFilePath() {
		BROWSEINFO bin;
		bin.hwndOwner = NULL;
		bin.pidlRoot = CSIDL_DESKTOP;
		bin.pszDisplayName = NULL;
		bin.lpszTitle = NULL;
		bin.ulFlags = BIF_DONTGOBELOWDOMAIN | BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;
		bin.lpfn = NULL;
		bin.iImage = 0;
		LPITEMIDLIST url = SHBrowseForFolder(&bin);
		if (url == NULL) {
			return "NULL";
		}
		TCHAR file[MAX_PATH];
		SHGetPathFromIDList(url, file);
		std::string return_ = this->convert(file);
		return return_;
	}
	_Post_ _Null_ HDC* NULLHDC() {
		return NULL;
	}
	void window::HIDECMD() {
		HWND HWnd = GetConsoleWindow();
		ShowWindow(HWnd,SW_HIDE);
	}
	void window::NO_MINI() {
		HWND hwnd = this->HWnd;
		HMENU hmenu = GetSystemMenu(hwnd, false);
		LONG style = GetWindowLong(hwnd, GWL_STYLE);
		style &= ~(WS_MINIMIZEBOX);
		SetWindowLong(hwnd, GWL_STYLE, style);
		SetWindowPos(hwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
		ShowWindow(hwnd, SW_SHOWNORMAL);
		DestroyMenu(hmenu);
		ReleaseDC(hwnd, (HDC)NULLHDC);
	}
	void window::NOSIZE() {
		LONG Style;
		Style = GetWindowLong(this->HWnd, GWL_STYLE);
		Style &= ~(WS_THICKFRAME | WS_MAXIMIZEBOX);
		SetWindowLong(this->HWnd, GWL_STYLE, Style);
	}
	void window::CQucikWindowThread() {
		this->NewWindow(this->x,this->y,this->px,this->py,this->convert(this->title));
		while (this->DoMessages()) {
			Sleep(80);
		}
	}
	void window::closegraph() {
		SendMessage(this->GetHWnd(),WM_CLOSE,0,0);
	}
	void window::initgraph(int x,int y,int px,int py) {
		this->x = x;
		this->y = y;
		this->px = px;
		this->py = py;
		this->WinPath = std::thread(&window::CQucikWindowThread,this);
		Sleep(80);
		this->HWnd = SWAP_HWND;
	}
	void window::initgraph(int x, int y) {
		this->x = x;
		this->y = y;
		this->px = GetSystemMetrics(SM_CXSCREEN) / 2 - (x / 2);
		this->py = GetSystemMetrics(SM_CYSCREEN) / 2 - (y / 2);
		this->WinPath = std::thread(&window::CQucikWindowThread, this);
		Sleep(80);
		this->HWnd = SWAP_HWND;
	}
	void window::SetHWnd(HWND hwnd) {
		this->HWnd = hwnd;
	}
	void window::FadeOut() {
		for (int i = 255; i >= 0; i--) {
			this->Transparent(i);
		}
	}
	void window::FadeIn() {
		for (int i = 0; i <= 255; i++) {
			this->Transparent(i);
		}
	}
	BOOL window::is_in(HWND HWnd) {
		RECT rect, rect_;
		GetWindowRect(HWnd, &rect);
		GetWindowRect(this->HWnd, &rect_);
		if (rect.left <= rect_.left
			and rect.bottom >= rect_.bottom
			and rect.top <= rect_.top and
			rect.right >= rect.right) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	HWND window::GetHWndForMouseAndReturn() {
		POINT MOUSE;
		if (GetCursorPos(&MOUSE)) {
			return WindowFromPoint(MOUSE);
		}
		else {
			return NULL;
		}
	}
	void window::move_to(HWND HWnd) {
		RECT rect;
		GetWindowRect(HWnd, &rect);
		this->move_to(rect.left, rect.top);
	}
	void window::PaintAgain() {
		SendMessage(this->HWnd, WM_PAINT, NULL, NULL);
	}
	void window::flicker() {
		SendMessage(FindWindow(0, 0), WM_SYSCOMMAND, SC_MONITORPOWER, 2);
		SendMessage(FindWindow(0, 0), WM_SYSCOMMAND, SC_MONITORPOWER, -1);
	}
	void window::NO_COLSE_ICON() {
		EnableMenuItem(GetSystemMenu(this->HWnd, FALSE), SC_CLOSE, MF_GRAYED);
	}
	void window::line(int x, int y) {
		LineTo(GetDC(this->HWnd), x, y);
	}
	void window::SetIcon(std::wstring string) {
		HICON hIcon, hIconSm;
		hIcon = (HICON)LoadImage(NULL, string.c_str(), IMAGE_ICON, 32, 32, LR_LOADFROMFILE);
		SendMessage(this->HWnd, WM_SETICON, ICON_BIG, (LPARAM)hIcon);
		hIconSm = (HICON)LoadImage(NULL, string.c_str(), IMAGE_ICON, 16, 16, LR_LOADFROMFILE);
		SendMessage(this->HWnd, WM_SETICON, ICON_SMALL, (LPARAM)hIconSm);
	}
	void window::HIDE_ICON() {
		SetWindowLong(this->HWnd, GWL_EXSTYLE, WS_EX_TOOLWINDOW);
	}
	void window::draw_circel(int x1, int x2, int y1, int y2) {
		Ellipse(GetDC(this->HWnd), x1, x2, y1, y2);
	}
	void window::mouse_is_cheak_title_box() {
		POINT m;
		GetCursorPos(&m);
		PostMessage(this->HWnd, WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(m.x, m.y));
	}
	void window::move_to(int x, int y) {
		RECT rect;
		GetWindowRect(this->HWnd, &rect);
		MoveWindow(this->HWnd, x, y, rect.right - rect.left, rect.bottom - rect.top, TRUE);
	}
	void window::DeleteTitleBox() {
		LONG Style;
		Style = GetWindowLong(this->HWnd, GWL_STYLE);
		Style = Style & ~WS_CAPTION;
		SetWindowLong(this->HWnd, GWL_STYLE, Style);
	}
	HWND window::GetHWnd() {
		return this->HWnd;
	}
	void window::GetHWndForMouse() {
		POINT MOUSE;
		if (GetCursorPos(&MOUSE)) {
			this->HWnd = WindowFromPoint(MOUSE);
		}
	}
	std::string window::__get_path__() {
		OPENFILENAME ofn;
		char szFile[300];
		ZeroMemory(&ofn, sizeof(ofn));
		ofn.lStructSize = sizeof(ofn);
		ofn.hwndOwner = NULL;
		ofn.lpstrFile = (LPWSTR)szFile;
		ofn.lpstrFile[0] = '\0';
		ofn.nFilterIndex = 1;
		ofn.nMaxFile = sizeof(szFile);
		ofn.lpstrFilter = L"ALL\0*.*\0Text\0*.TXT\0";
		ofn.lpstrFileTitle = NULL;
		ofn.nMaxFileTitle = 0;
		ofn.lpstrInitialDir = NULL;
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
		std::string path_image = "";
		if (GetOpenFileName(&ofn)) {
			path_image = this->convert(ofn.lpstrFile);
			return path_image;
		}
		else {
			return "";
		}
	}
	BOOL window::Animate(DWORD Time, DWORD style) {
		BOOL RETURN = AnimateWindow(this->HWnd, Time, style);
		return RETURN;
	}
	void window::TOP() {
		RECT rect;
		GetWindowRect(this->HWnd, &rect);
		SetWindowPos(this->HWnd, HWND_TOPMOST, rect.left, rect.top, 0, 0, SWP_NOSIZE);
	}
	void window::SHOW() {
		ShowWindow(this->HWnd, SW_RESTORE);
	}
	void window::HIDE() {
		ShowWindow(this->HWnd, SW_HIDE);
	}
	void window::REDUCTION() {
		ShowWindow(this->HWnd, SW_RESTORE);
	}
	void window::MINI() {
		ShowWindow(this->HWnd, SW_SHOWMAXIMIZED);
	}
	void window::MAX() {
		ShowWindow(this->HWnd, SW_SHOWMINIMIZED);
	}
	BOOL window::SetHWND(std::wstring string) {
		if (FindWindow(NULL, string.c_str()) != NULL) {
			this->HWnd = FindWindow(NULL, string.c_str());
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	void window::SetCursorIcon(LPCWSTR icon) {
		HMODULE hmod = GetModuleHandle(NULL);
		HCURSOR hcur = (HCURSOR)LoadImage(NULL, icon, IMAGE_CURSOR, 0, 0, LR_LOADFROMFILE);
		SetClassLong(this->HWnd, GCL_HCURSOR, (long)hcur);
	}
	void window::Transparent(COLORREF Mask) {
		SetWindowLong(this->HWnd, GWL_EXSTYLE, GetWindowLong(this->HWnd, GWL_EXSTYLE) | WS_EX_LAYERED);
		SetLayeredWindowAttributes(this->HWnd, Mask, 0, LWA_COLORKEY);
	}
	void window::Transparent(int Transparent_num) {
		SetWindowLong(this->HWnd, GWL_EXSTYLE, GetWindowLong(this->HWnd, GWL_EXSTYLE) | WS_EX_LAYERED);
		SetLayeredWindowAttributes(this->HWnd, 0, Transparent_num, LWA_ALPHA);
	}
	void window::RounWindow(int x1, int x2, int x3, int x4) {
		HRGN HRgn = CreateEllipticRgn(x1, x2, x3, x4);
		SetWindowRgn(this->HWnd, HRgn, true);
	}
	void window::move(int x, int y) {
		RECT rect;
		GetWindowRect(this->HWnd, &rect);
		MoveWindow(this->HWnd, rect.left + x, rect.top + y, rect.right - rect.left, rect.bottom - rect.top, TRUE);
	}
	void window::EXIT(int return_) {
		PostMessage(this->HWnd, WM_DESTROY, 0, 0);
		exit(return_);
	}
	void window::Flash(bool Backstage) {
		::FlashWindow(this->HWnd, Backstage);
	}
	window::window(std::string string) {
		this->title = string.c_str();
	}
	window::window(HWND HWnd) {
		this->HWnd = HWnd;
	}
	window::window() {
		this->SetTitle();
		this->HWnd = GetForegroundWindow();
		SetWindowText(this->HWnd, this->convert(this->title).c_str());
	}
	BOOL window::SetTitle() {
		if (this->HWnd != NULL) {
			SetWindowText(this->HWnd, this->convert(this->title).c_str());
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
};

