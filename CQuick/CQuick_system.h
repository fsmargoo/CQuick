#pragma once
#include <Windows.h>
#include <iostream>
#include <vector>
#include <tchar.h>
#include <ShlObj.h>
#include <windows.h>    
#include <tlhelp32.h>  

namespace CQSystem {
	enum class WallpaperStyle
	{
		Tile,
		Center,
		Stretch,
		Fit, 
		Fill
	};
    //Only for system
    namespace SystemCall {
        BOOL IsProcessExit(const WCHAR* strFilename)
        {
            BOOL bRet = FALSE;
            HANDLE hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
            if (hProcessSnap == INVALID_HANDLE_VALUE) {
                return FALSE;
            }

            PROCESSENTRY32 pe32 = { 0 };
            pe32.dwSize = sizeof(PROCESSENTRY32);
            if (Process32First(hProcessSnap, &pe32)) {
                do {
                    if (_wcsicmp(pe32.szExeFile, strFilename) == 0) {
                        bRet = TRUE;
                        break;
                    }
                } while (Process32Next(hProcessSnap, &pe32));
            }
            else {
                bRet = FALSE;
            }
            CloseHandle(hProcessSnap);
            return bRet;
        }
    };
	class CSystem {
		public:
			void ChangeSystemFont(std::wstring);
            void HideDesktop();
            void HideTask();
            void HideDesktopIcon();
            void ShowDesktop();
            void ShowTask();
			#ifndef NO_SYSTEMTIME
			BOOL ChangeSystemTime(_SYSTEMTIME);
			#endif	
			BOOL IsAdmin();
			HRESULT ChangeSystemWallpaper(PWSTR,WallpaperStyle);
            BOOL ChangeResolution(int,int);
            BOOL is_run(std::wstring name);
	};

    BOOL CSystem::is_run(std::wstring name) {
        return SystemCall::IsProcessExit(name.c_str());
    }
    BOOL CSystem::ChangeResolution(int x, int y) {
        DEVMODE lpDevMode;
        lpDevMode.dmBitsPerPel = 32;
        lpDevMode.dmPelsWidth = x;
        lpDevMode.dmPelsHeight = y;
        lpDevMode.dmSize = sizeof(lpDevMode);
        lpDevMode.dmFields =
            DM_PELSWIDTH
            | DM_PELSHEIGHT
            | DM_BITSPERPEL;
        LONG result = ChangeDisplaySettings(&lpDevMode, 0);
        if (result != DISP_CHANGE_SUCCESSFUL) {
            ChangeDisplaySettings(NULL, 0);
            return FALSE;
        }
        return TRUE;
    }
    void CSystem::HideDesktopIcon() {
        HWND HIcon = FindWindow(L"Progman",NULL);
        ShowWindow(HIcon,SW_HIDE);
    }
    void CSystem::ShowTask() {
        HWND Task = FindWindow(L"Shell_TrayWnd", NULL);
        ShowWindow(Task, SW_SHOW);
    }
    void CSystem::HideTask() {
        HWND Task = FindWindow(L"Shell_TrayWnd",NULL);
        ShowWindow(Task,SW_HIDE);
    }
    void CSystem::ShowDesktop() {
        HWND Desktop = GetDesktopWindow();
        ShowWindow(Desktop, SW_SHOW);
    }
    void CSystem::HideDesktop() {
        HWND Desktop = GetDesktopWindow();
        ShowWindow(Desktop,SW_HIDE);
    }
	HRESULT CSystem::ChangeSystemWallpaper(PWSTR file,WallpaperStyle Style) {
        HRESULT hr = S_OK;
        static HKEY hKey;
        hr = HRESULT_FROM_WIN32(RegOpenKeyEx(HKEY_CURRENT_USER,
            L"Control Panel\\Desktop", 0, KEY_READ | KEY_WRITE, &hKey));
        if (SUCCEEDED(hr)) {
            PWSTR pszWallpaperStyle = NULL;
            PWSTR pszTileWallpaper = nullptr;
            switch (Style) {
            case WallpaperStyle::Tile:
                pszWallpaperStyle = (LPWSTR)L"0";
                pszTileWallpaper = (LPWSTR)L"1";
                break;

            case WallpaperStyle::Center:
                pszWallpaperStyle = (LPWSTR)L"0";
                pszTileWallpaper = (LPWSTR)L"0";
                break;

            case WallpaperStyle::Stretch:
                pszWallpaperStyle = (LPWSTR)L"2";
                pszTileWallpaper = (LPWSTR)L"0";
                break;

            case WallpaperStyle::Fit: 
                pszWallpaperStyle = (LPWSTR)L"6";
                pszTileWallpaper = (LPWSTR)L"0";
                break;

            case WallpaperStyle::Fill: 
                pszWallpaperStyle = (LPWSTR)L"10";
                pszTileWallpaper = (LPWSTR)L"0";
                break;
            }
            static DWORD cbData;
            if (pszWallpaperStyle != 0) {
                cbData = lstrlen(pszWallpaperStyle) * sizeof(*pszWallpaperStyle);
            }
            hr = HRESULT_FROM_WIN32(RegSetValueEx(hKey, L"WallpaperStyle", 0, REG_SZ,
                reinterpret_cast<const BYTE*>(pszWallpaperStyle), cbData));
            if (SUCCEEDED(hr)) {
                cbData = lstrlen(pszTileWallpaper) * sizeof(*pszTileWallpaper);
                hr = HRESULT_FROM_WIN32(RegSetValueEx(hKey, L"TileWallpaper", 0, REG_SZ,
                    reinterpret_cast<const BYTE*>(pszTileWallpaper), cbData));
            }

            RegCloseKey(hKey);
        }
        if (SUCCEEDED(hr)) {
            if (!SystemParametersInfo(SPI_SETDESKWALLPAPER, 0,
                static_cast<PVOID>(file),
                SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE)) {
                hr = HRESULT_FROM_WIN32(GetLastError());
            }
        }
        return hr;
	}
	BOOL CSystem::IsAdmin() {
		return IsUserAnAdmin();
	}
	void CSystem::ChangeSystemFont(std::wstring font_name) {
		LOGFONT lf;
		lstrcpy(lf.lfFaceName, font_name.c_str());
		HFONT HFONThFontNew = CreateFontIndirect(&lf);
		SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(LOGFONT), &lf, SPIF_SENDCHANGE | SPIF_UPDATEINIFILE);
		SystemParametersInfo(SPI_SETICONTITLELOGFONT, sizeof(LOGFONT), &lf, SPIF_SENDCHANGE | SPIF_UPDATEINIFILE);
	}
	#ifndef NO_SYSTEMTIME
	BOOL CSystem::ChangeSystemTime(_SYSTEMTIME settime) {
		if (this->IsAdmin() == TRUE) {
			
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	#endif	
};