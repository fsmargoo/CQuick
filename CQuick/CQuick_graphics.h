#pragma once
#include <Windows.h>
#include <gdiplus.h>
#include <gdipluspen.h>
#include <iostream>
#pragma comment(lib, "gdiplus")
#define Loadimage LoadImageW
namespace CQGraphics {
    namespace CQApi {
        int y = 0;
        HWND hWnd;
    }
    
    void SetHWND(HWND h) {
        CQApi::hWnd = h;
    }
    Gdiplus::Color GetColor(BYTE r, BYTE g, BYTE b) {
        return Gdiplus::Color(r,g,b);
    }
    Gdiplus::Color GetColor(BYTE a, BYTE r, BYTE g, BYTE b) {
        return Gdiplus::Color(a,r, g, b);
    }
	BOOL LoadImageW(std::wstring url) {
		 using namespace Gdiplus;
		 Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
		 ULONG_PTR m_pGdiToken;
		 Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
         HDC hdc = GetDC(CQApi::hWnd);
         Image image(url.c_str());
         if (image.GetLastStatus() != Status::Ok) {
             return FALSE;  
         }
         int w = image.GetWidth(), h = image.GetHeight();
         Graphics graphics(hdc);
         graphics.DrawImage(&image,0,0,w,h);
         return TRUE;
	}
    BOOL LoadImageW(std::wstring url,int x,int y) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        HDC hdc = GetDC(CQApi::hWnd);
        Image image(url.c_str());
        if (image.GetLastStatus() != Status::Ok) {
            return FALSE;
        }
        int w = image.GetWidth(), h = image.GetHeight();
        Graphics graphics(hdc);
        graphics.DrawImage(&image, x, y, w, h);
        return TRUE;
    }
    BOOL LoadImageW(std::wstring url, int x, int y, int w, int h) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        HDC hdc = GetDC(CQApi::hWnd);
        Image image(url.c_str());
        if (image.GetLastStatus() != Status::Ok) {
            return FALSE;
        }
        Graphics graphics(hdc);
        graphics.DrawImage(&image, x, y, w, h);
        return TRUE;
    }
    BOOL CutImageW(std::wstring url, int x1, int y1, int x2, int y2) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        Image image(url.c_str());
        graphics.DrawImage(&image, 0, 0, x1, y1, x2 - x1, y2 - y1, UnitPixel);
        return TRUE;
    }
    BOOL LoadImageW(std::wstring url, int degree) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        if (degree == 360) {
            degree = 10;
        }
        else {
            degree += 10;
        }
        HDC dc = ::GetDC(CQApi::hWnd);
        Image image(url.c_str());
        int nWeight = image.GetWidth();
        int nHeight = image.GetHeight();
        graphics.TranslateTransform(float(nWeight / 2), float(nHeight / 2));
        graphics.RotateTransform((Gdiplus::REAL)degree);
        graphics.DrawImage(&image, -(nWeight / 2), -(nHeight / 2), nWeight, nHeight);
        ::ReleaseDC(CQApi::hWnd, GetDC((CQApi::hWnd)));
        return TRUE;
    }
    void Line(int x1, int y1, int x2, int y2) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Pen pen(Color(0,0,0),3);
        PointF PTStart((Gdiplus::REAL)x1,(Gdiplus::REAL)y1);
        PointF PTSEnd((Gdiplus::REAL)x2, (Gdiplus::REAL)y2);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        graphics.DrawLine(&pen,PTStart,PTSEnd);
    }
    void Line(int x1, int y1, int x2, int y2, Gdiplus::Color color) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Pen pen(color, 3);
        PointF PTStart((Gdiplus::REAL)x1, (Gdiplus::REAL)y1);
        PointF PTSEnd((Gdiplus::REAL)x2, (Gdiplus::REAL)y2);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        graphics.DrawLine(&pen, PTStart, PTSEnd);
    }
    void point(int x, int y) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Pen pen(Color(0,0,0), 3);
        PointF PTStart((Gdiplus::REAL)x, (Gdiplus::REAL)y); 
        PointF PTSEnd((Gdiplus::REAL)x + 1, (Gdiplus::REAL)y + 1);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        graphics.DrawLine(&pen, PTStart, PTSEnd);
    }
    void point(int x, int y,Gdiplus::Color color) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Pen pen(color, 3);
        PointF PTStart((Gdiplus::REAL)x, (Gdiplus::REAL)y);
        PointF PTSEnd((Gdiplus::REAL)x + 1, (Gdiplus::REAL)y + 1);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        graphics.DrawLine(&pen, PTStart, PTSEnd);
    }
    void Cable(Gdiplus::PointF* Points,int size) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        Pen blackPen(Color(255, 0, 0, 0), 3);
        graphics.DrawLines(&blackPen,Points,size);
    }
    void Cable(Gdiplus::PointF* Points, int size,Gdiplus::Color color) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        Pen blackPen(color, 3);
        graphics.DrawLines(&blackPen, Points, size);
    }
    void Rectangles(int x1,int y1,int x2,int y2) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        Pen pen(Color(255,0,0,0),3);
        Rect rect(x1,y1,x2,y2);
        graphics.DrawRectangle(&pen,rect);
    }
    void Rectangles(int x1, int y1, int x2, int y2,Gdiplus::Color color) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        Pen pen(color, 3);
        Rect rect(x1, y1, x2, y2);
        graphics.DrawRectangle(&pen, rect);
    }
    void Curve(Gdiplus::PointF* Point,int size) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        Pen pen(Color(0,0,0),3);
        graphics.DrawCurve(&pen, Point, size);
    }
    void Curve(Gdiplus::PointF* Point, int size,Gdiplus::Color color) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        Pen pen(color, 3);
        graphics.DrawCurve(&pen, Point, (INT)size);
    }
    void Polygon(Gdiplus::PointF* Point, int size) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        Pen pen(Color(0,0,0), 3);
        graphics.DrawPolygon(&pen, Point, (INT)size);
    }
    void Arc(Gdiplus::REAL start, Gdiplus::REAL end,Gdiplus::REAL x1, Gdiplus::REAL y1, Gdiplus::REAL x2, Gdiplus::REAL y2) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        Pen redPen(Color(0,0,0), 3);
        RectF ellipseRect(x1, y1, x2, y2);
        REAL startAngle = start;
        REAL sweepAngle = end;
        graphics.DrawArc(&redPen, ellipseRect, startAngle, sweepAngle);
    }
    void Arc(Gdiplus::REAL start, Gdiplus::REAL end, Gdiplus::REAL x1, Gdiplus::REAL y1, Gdiplus::REAL x2, Gdiplus::REAL y2,Gdiplus::Color color) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        Pen redPen(color, 3);
        RectF ellipseRect(x1, y1, x2, y2);
        REAL startAngle = start;
        REAL sweepAngle = end;
        graphics.DrawArc(&redPen, ellipseRect, startAngle, sweepAngle);
    }
    void Pie(Gdiplus::REAL start, Gdiplus::REAL end, Gdiplus::REAL x1, Gdiplus::REAL y1, Gdiplus::REAL x2, Gdiplus::REAL y2) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        Pen Pen(Color(0, 0, 0), 3);
        RectF ellipseRect(x1, y1, x2, y2);
        REAL startAngle = start;
        REAL sweepAngle = end;
        graphics.DrawArc(&Pen, ellipseRect, startAngle, sweepAngle);
    }
    void Pie(Gdiplus::REAL start, Gdiplus::REAL end, Gdiplus::REAL x1, Gdiplus::REAL y1, Gdiplus::REAL x2, Gdiplus::REAL y2, Gdiplus::Color color) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        Pen redPen(color, 3);
        RectF ellipseRect(x1, y1, x2, y2);
        REAL startAngle = start;
        REAL sweepAngle = end;
        graphics.DrawArc(&redPen, ellipseRect, startAngle, sweepAngle);
    }
    void Ellipse(Gdiplus::REAL x1, Gdiplus::REAL y1, Gdiplus::REAL x2, Gdiplus::REAL y2) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        Pen pen(Color(0, 0, 0), 3);
        RectF ellipse(x1,y1,x2,y2);
        graphics.DrawEllipse(&pen,x1,y1,x2,y2);
    }
    void Ellipse(Gdiplus::REAL x1, Gdiplus::REAL y1, Gdiplus::REAL x2, Gdiplus::REAL y2,Gdiplus::Color color) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        Pen pen(color, 3);
        RectF ellipse(x1, y1, x2, y2);
        graphics.DrawEllipse(&pen, x1, y1, x2, y2);
    }
    void FillEllipse(Gdiplus::REAL x1, Gdiplus::REAL y1, Gdiplus::REAL x2, Gdiplus::REAL y2, Gdiplus::Color color, Gdiplus::Color color_,BOOL frame = TRUE) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        SolidBrush blackBrush(color);
        RectF ellipseRect(x1, y1, x2, y2);
        graphics.FillEllipse(&blackBrush, ellipseRect);
        if (frame == TRUE) {
            Pen borderPen(color_, 3);
            graphics.DrawEllipse(&borderPen, ellipseRect);
        }
    }
    void Fillcurve(Gdiplus::PointF* point, Gdiplus::Color color, Gdiplus::Color color_,int size, BOOL frame = TRUE) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        SolidBrush brush(color);
        graphics.FillClosedCurve(&brush,point,size);
        if (frame == TRUE) {
            Pen pen(color_, (Gdiplus::REAL)size);
            graphics.DrawClosedCurve(&pen, point, size);
        }
    }
    void FillPie(Gdiplus::REAL x1, Gdiplus::REAL y1, Gdiplus::REAL x2, Gdiplus::REAL y2,Gdiplus::REAL start, Gdiplus::REAL end, Gdiplus::Color color, Gdiplus::Color color_, BOOL frame = TRUE) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        SolidBrush brush(color);
        RectF FillPie(x1,y1,x2,y2);
        graphics.FillEllipse(&brush,FillPie);
        if (frame == TRUE) {
            Pen pen(color_);
            graphics.DrawPie(&pen, FillPie, start, end);
        }
    }
    void FillPloygon(Gdiplus::PointF* Point, int size, Gdiplus::Color color, Gdiplus::Color color_, BOOL frame = TRUE) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        SolidBrush brush(color);
        graphics.FillPolygon(&brush,Point,(INT)size);
        if (frame == TRUE) {
            Pen pen(color_,3);
            graphics.DrawPolygon(&pen,Point,(INT)size);
        }
    }
    void FillRectangle(Gdiplus::REAL x1, Gdiplus::REAL y1, Gdiplus::REAL x2, Gdiplus::REAL y2, Gdiplus::Color color, Gdiplus::Color color_, BOOL frame = TRUE) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        SolidBrush brush(color);
        RectF FillRectangle(x1, y1, x2, y2);
        graphics.FillRectangle(&brush,FillRectangle);
        if (frame == TRUE) {
            Pen pen(color_);
            Rect rect((INT)x1, (INT)y1,(INT)x2,(INT)y2);
            graphics.DrawRectangle(&pen,rect);
        }
    }
    void outtext(std::wstring text, HWND hWnd) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        SolidBrush brush(Color(0, 0, 0));
        FontFamily fontfamily(L"����");
        Font font(&fontfamily, 24, FontStyleRegular, UnitPixel);
        PointF pointf((Gdiplus::REAL)0, (Gdiplus::REAL)CQApi::y);
        CQApi::y = CQApi::y + 24;
        graphics.DrawString(text.c_str(), -1, &font, pointf, &brush);
    }
    void outtext(std::wstring text, std::wstring FontName) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        SolidBrush brush(Color(0, 0, 0));
        FontFamily fontfamily(FontName.c_str());
        Font font(&fontfamily, 24, FontStyleRegular, UnitPixel);
        PointF pointf((Gdiplus::REAL)0, (Gdiplus::REAL)CQApi::y);
        CQApi::y = CQApi::y + 24;
        graphics.DrawString(text.c_str(), -1, &font, pointf, &brush);
    }
    void outtext(std::wstring text, std::wstring FontName, Gdiplus::REAL size) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        SolidBrush brush(Color(0, 0, 0));
        FontFamily fontfamily(FontName.c_str());
        Font font(&fontfamily, size, FontStyleRegular, UnitPixel);
        PointF pointf((Gdiplus::REAL)0, (Gdiplus::REAL)CQApi::y);
        CQApi::y = CQApi::y + (int)size;
        graphics.DrawString(text.c_str(), -1, &font, pointf, &brush);
    }
    void outtext(std::wstring text, std::wstring FontName, int size, Gdiplus::Color color) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        SolidBrush brush(color);
        FontFamily fontfamily(FontName.c_str());
        Font font(&fontfamily, (Gdiplus::REAL)size, FontStyleRegular, UnitPixel);
        PointF pointf((Gdiplus::REAL)0, (Gdiplus::REAL)CQApi::y);
        CQApi::y = CQApi::y + size;
        graphics.DrawString(text.c_str(), -1, &font, pointf, &brush);
    }
    void outtext(std::wstring text, std::wstring FontName, int size, Gdiplus::Color color, int x, int y) {
        using namespace Gdiplus;
        Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
        ULONG_PTR m_pGdiToken;
        Gdiplus::GdiplusStartup(&m_pGdiToken, &m_gdiplusStartupInput, NULL);
        Graphics graphics(GetDC(CQApi::hWnd));
        graphics.SetSmoothingMode(SmoothingModeHighQuality);
        graphics.SetTextRenderingHint(TextRenderingHintSingleBitPerPixel);
        SolidBrush brush(color);
        FontFamily fontfamily(FontName.c_str());
        Font font(&fontfamily, (Gdiplus::REAL)size, FontStyleRegular, UnitPixel);
        PointF pointf((Gdiplus::REAL)x, (Gdiplus::REAL)y);
        graphics.DrawString(text.c_str(), -1, &font, pointf, &brush);
    }
}