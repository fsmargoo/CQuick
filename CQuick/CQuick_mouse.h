#pragma once
#include <windows.h>
#include <iostream>

namespace CQMouse {
#define MOUSE_LEFT  "Left"
#define MOSUE_RIGHT "Right"
#define MOUSE_AMID  "AMid"
#define KEY_DOWN(VK_NONAME) ((GetAsyncKeyState(VK_NONAME) & 0x8000) ? 1:0)
	class MOUSEMSG {
	public:
		BOOL MRight   = FALSE;
		BOOL MLeft    = FALSE;
		BOOL MAmid	  = FALSE;
		int x         = 0;
		int y		  = 0;
		int x_desktop = 0;
		int y_desktop = 0;
		void GetMouseMsg();
		void move(int,int);
		void move_to(int,int);
		void AmidDown();
		void AmidUp();
		void LeftDown();
		void LeftUp();
		void RightDown();
		void RightUp();
		void On_Click(void* function(std::string));
	};


	void MOUSEMSG::AmidUp() {
		mouse_event(MOUSEEVENTF_MIDDLEUP, 0, 0, 0, 0);
	}
	void MOUSEMSG::AmidDown() {
		mouse_event(MOUSEEVENTF_MIDDLEDOWN, 0, 0, 0, 0);
	}
	void MOUSEMSG::RightUp() {
		mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
	}
	void MOUSEMSG::RightDown() {
		mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
	}
	void MOUSEMSG::LeftUp() {
		mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
	}
	void MOUSEMSG::LeftDown() {
		mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
	}
	void MOUSEMSG::move_to(int x, int y) {
		SetCursorPos(x,y);
	}
	void MOUSEMSG::move(int x,int y) {
		POINT cur;
		GetCursorPos(&cur);
		SetCursorPos(cur.x + x,cur.y + y);
	}
	void MOUSEMSG::GetMouseMsg() {
		if (KEY_DOWN(VK_LBUTTON)) {
			this->MLeft = TRUE;
		} 
		else {
			this->MLeft = FALSE;
		}
		if (KEY_DOWN(VK_RBUTTON)) {
			this->MRight = TRUE;
		}
		else {
			this->MRight = FALSE;
		}
		if (KEY_DOWN(VK_MBUTTON)) {
			this->MAmid  = TRUE;
		}
		else {
			this->MAmid  = FALSE;
		}
		POINT CurSorPoint;
		GetCursorPos(&CurSorPoint);
		this->x_desktop = CurSorPoint.x;
		this->y_desktop = CurSorPoint.y;
		ScreenToClient(GetForegroundWindow(),&CurSorPoint);
		this->x = CurSorPoint.x;
		this->y = CurSorPoint.y;
	}
	void MOUSEMSG::On_Click(void *function(std::string)) {
		MOUSEMSG msg;
		msg.GetMouseMsg();
		if (msg.MAmid == TRUE) {
			if (msg.MAmid == TRUE) {
				function("AMid");
			}
		}
		else if (msg.MLeft == TRUE) {
			if (msg.MLeft == TRUE) {
				function("Left");
			}
		}
		else if (msg.MRight == TRUE) {
			if (msg.MRight == TRUE) {
				function("Right");
			}
		}
	}
}