#pragma once
#include <Windows.h>
#include <stdio.h>
#include <iostream>
#pragma comment(lib, "winmm.lib") 

namespace CQRecording {
	class Recording {
	public:
		void recording(int,std::string);
	};
	void Recording::recording(int time,std::string fileurl) {
        HWAVEIN      hWaveIn;  
        WAVEFORMATEX waveform; 
        BYTE*        pBuffer1;
        WAVEHDR      wHdr1; 
        FILE*        pf;
        HANDLE       wait;
        waveform.wFormatTag = WAVE_FORMAT_PCM;
        waveform.nSamplesPerSec  = 8000;
        waveform.wBitsPerSample  = 16;
        waveform.nChannels       = 1;
        waveform.nAvgBytesPerSec = 16000;
        waveform.nBlockAlign     = 2;
        waveform.cbSize          = 0;
        wait = CreateEvent(NULL, 0, 0, NULL);
        waveInOpen(&hWaveIn, WAVE_MAPPER, &waveform, (DWORD_PTR)wait, 0L, CALLBACK_EVENT);
        DWORD bufsize = 1024 * 100;
        fopen_s(&pf, fileurl.c_str() , "wb");
        while (time--) {
            pBuffer1 = new BYTE[bufsize];
            wHdr1.lpData = (LPSTR)pBuffer1;
            wHdr1.dwBufferLength = bufsize;
            wHdr1.dwBytesRecorded = 0;
            wHdr1.dwUser  = 0;
            wHdr1.dwFlags = 0;
            wHdr1.dwLoops = 1;
            waveInPrepareHeader(hWaveIn, &wHdr1, sizeof(WAVEHDR));
            waveInAddBuffer(hWaveIn, &wHdr1, sizeof(WAVEHDR));
            waveInStart(hWaveIn);
            Sleep(1000);
            waveInReset(hWaveIn);
            if (pf != 0) {
                fwrite(pBuffer1, 1, wHdr1.dwBytesRecorded, pf);
            }
            delete[] pBuffer1;
        }
        if (pf != 0) {
            fclose(pf);
        }
        waveInClose(hWaveIn);
	}
}