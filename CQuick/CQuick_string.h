#pragma once
#include <atlstr.h>
#include <string>
#include <string.h>
#include <iostream>
#include <fstream>
#include <Windows.h>
#include <vector>
#include <sstream>
#include <comdef.h>
#include <comutil.h>
#include <locale.h>
#include <commdlg.h>
namespace CQString {
	const char base[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	using ReadPermissionBool = bool;

	/*class character to store stringsn
	  His calling method is called by '.', E.g.
	  demo.size();
	  If you do n��t have some experience, do n��t change this*/
	class character {
	private:
		// For system calls
		int push_numer = 0;
		char* base64_encode(const char* data, int data_len) {
			int prepare = 0;
			int ret_len;
			int temp = 0;
			char* ret = NULL;
			char* f = NULL;
			int tmp = 0;
			char changed[4];
			int i = 0;
			ret_len = data_len / 3;
			temp = data_len % 3;
			if (temp > 0) {
				ret_len += 1;
			}
			ret_len = ret_len * 4 + 1;
			ret = (char*)malloc(ret_len);
			if (ret == NULL) {
				return nullptr;
			}
			memset(ret, 0, ret_len);
			f = ret;
			while (tmp < data_len) {
				temp = 0;
				prepare = 0;
				memset(changed, '\0', 4);
				while (temp < 3) {
					if (tmp >= data_len) {
						break;
					}
					prepare = ((prepare << 8) | (data[tmp] & 0xFF));
					tmp++;
					temp++;
				}
				prepare = (prepare << ((3 - temp) * 8));
				for (i = 0; i < 4; i++) {
					if (temp < i) {
						changed[i] = 0x40;
					}
					else {
						changed[i] = (prepare >> ((3 - i) * 6)) & 0x3F;
					}
					*f = base[changed[i]];
					f++;
				}
			}
			*f = '\0';
			return ret;
		}
		static char find_pos(char ch) {
			char* ptr = (char*)strrchr(base, ch);
			return (ptr - base);
		}
		char* base64_decode(const char* data, int data_len) {
			int ret_len = (data_len / 4) * 3;
			int equal_count = 0;
			char* ret = NULL;
			char* f = NULL;
			int tmp = 0;
			int temp = 0;
			int prepare = 0;
			int i = 0;
			if (*(data + data_len - 1) == '=') {
				equal_count += 1;
			}
			if (*(data + data_len - 2) == '=') {
				equal_count += 1;
			}
			if (*(data + data_len - 3) == '=') {
				equal_count += 1;
			}
			switch (equal_count) {
			case 0:
				ret_len += 4;
				break;
			case 1:
				ret_len += 4;
				break;
			case 2:
				ret_len += 3;
				break;
			case 3:
				ret_len += 2;
				break;
			}
			ret = (char*)malloc(ret_len);
			if (ret == NULL) {
				return nullptr;
			}
			memset(ret, 0, ret_len);
			f = ret;
			while (tmp < (data_len - equal_count)) {
				temp = 0;
				prepare = 0;
				while (temp < 4) {
					if (tmp >= (data_len - equal_count)) {
						break;
					}
					prepare = (prepare << 6) | (find_pos(data[tmp]));
					temp++;
					tmp++;
				}
				prepare = prepare << ((4 - temp) * 6);
				for (i = 0; i < 3; i++) {
					if (i == temp) {
						break;
					}
					*f = (char)((prepare >> ((2 - i) * 8)) & 0xFF);
					f++;
				}
			}
			*f = '\0';
			return ret;
		}
		std::string convert(LPCSTR string) {
			return std::string(string);
		}
		char* convert(LPCTSTR string) {
			int num = WideCharToMultiByte(CP_OEMCP, NULL, string, -1, NULL, 0, NULL, FALSE);
			char* pchar = new char[num];
			WideCharToMultiByte(CP_OEMCP, NULL, string, -1, pchar, num, NULL, FALSE);
			return pchar;
		}
		std::string convert(CString string) {
			std::string result(string.GetLength(), 'e');
			for (int i = 0; i < string.GetLength(); i++) {
				result[i] = (char)string[i];
			}
			return result;
		}
		std::string convert(char* string) {
			return std::string(string);
		}
		std::string convert(std::wstring string) {
			_bstr_t t = string.c_str();
			char* pchar = (char*)t;
			std::string result = pchar;
			return result;
		}
		std::wstring convert(const std::string& string) {
			_bstr_t t = string.c_str();
			wchar_t* swap_char = (wchar_t*)t;
			std::wstring return_ = swap_char;
			return return_;
		}
	protected:
		std::string character_string;
		ReadPermissionBool ReadPermission = true;// Read permission, if false, cannot write and read
	public:
		void encryption();
		void Decrypt();
		bool empty();
		friend std::ostream& operator<<(std::ostream& output, const character& str);
		friend bool operator<(const character& str1, const character& str2);
		friend bool operator==(const character& str1, const character& str2);
		character operator+(const character&);
		character operator=(const std::wstring&);
		character operator=(const std::string&);
		character operator=(const LPCWSTR&);
		character operator=(const TCHAR&);
		friend bool operator>(const character& str1, const character& str2);
		friend bool operator!=(const character& str1, const character& str2);
		friend std::istream& operator>>(std::istream& input, character& str);
		char& operator[](unsigned int length);
		character(int push_string);
		character(long long push_string);
		character(LPCSTR push_string);
#ifndef _UNICODE
		void UnicodeToAnis(const std::wstring);
#endif
		character(const std::wstring& push_string);
		character(double push_string);
		character(char* push_string);
		character(CString push_string);
		character();
		void pop(std::string);
		void SetReadPermissions(bool);
		void __Setlength__(int);
		int __return__int();
		char __Rigthcharacter__(int);
		std::string __return__string();
		template<typename  TypeEqualTemplate>
		BOOL TypeEqual(TypeEqualTemplate, TypeEqualTemplate);
		std::wstring __return__wstring();
		double __return__double();
		int find(std::string, int);
		void __Get_clipboard_data__();
		void __GetCommandWindowReturn__(const std::string&);
		int find(std::string);
		int find(int, std::string);
		void replace(std::string, std::string);
		void get(bool);
		void get(bool, std::string);
		void Expansion(int ExpansionNum);
		void destroy();
		void InitializationString();
		void Case();
		void uppercase();
		void lowercase();
		void Flip();
		std::string GetExtension();
		std::string slice(int, int);
		unsigned int length();
		unsigned int size();
		std::string* GetAddress();
		void exchange(int, int);
		std::string return_character_string();
		void pop();
	};
#ifndef _UNICODE
	void character::UnicodeToAnis(std::wstring string) {
		char* pElementText;
		int    iTextLen;
		iTextLen = WideCharToMultiByte(CP_ACP, 0,
			str.c_str(),
			-1,
			nullptr,
			0,
			nullptr,
			nullptr);
		pElementText = new char[iTextLen + 1];
		memset((void*)pElementText, 0, sizeof(char) * (iTextLen + 1));
		::WideCharToMultiByte(CP_ACP,
			0,
			str.c_str(),
			-1,
			pElementText,
			iTextLen,
			nullptr,
			nullptr);
		std::string strText;
		strText = pElementText;
		delete[] pElementText;
		this->character_string = strText;
	}
#endif
	template<typename  TypeEqualTemplate>
	BOOL character::TypeEqual(TypeEqualTemplate String, TypeEqualTemplate String2) {
		if (typeid(String) == typeid(String2)) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	character character::operator=(const TCHAR& string) {
		character return__;
		return__.character_string = this->convert(string);
		return return__;
	}
	character character::operator=(const LPCWSTR& string) {
		character return__;
		return__.character_string = this->convert(string);
		return return__;
	}
	character character::operator=(const std::string& string) {
		character return__;
		return__.character_string = string;
		return return__;
	}
	void character::exchange(int Subscript_one, int Subscript_tow) { // Swap two strings (within an array, if you want to swap manually outside the array)
		char temp = this->character_string[Subscript_one];
		this->character_string[Subscript_one] = this->character_string[Subscript_tow];
		this->character_string[Subscript_tow] = temp;
	}
	void character::__GetCommandWindowReturn__(const std::string& command) { // Get the return value of the command window
		char buf[10240] = { 0 };
		FILE* pf = NULL;
		if ((pf = _popen(command.c_str(), "r")) == NULL)
		{
			this->character_string = " ";
		}
		std::string return__;
		if (pf != 0) {
			while (fgets(buf, sizeof buf, pf))
			{
				return__ += buf;
			}
		}
		if (pf != 0) {
			_pclose(pf);
		}

		unsigned int iSize = return__.size();
		if (iSize > 0 && return__[iSize - 1] == '\n')
		{
			return__ = return__.substr(0, iSize - 1);
		}
		this->character_string = return__;
	}
	void character::__Get_clipboard_data__() { // Get string in clipboard
		HWND hWnd = NULL;
		OpenClipboard(hWnd);
		char* temp = nullptr;
		if (IsClipboardFormatAvailable(CF_TEXT))
		{
			HANDLE h = GetClipboardData(CF_TEXT);
			temp = (char*)GlobalLock(h);
			GlobalUnlock(h);
		}
		CloseClipboard();
		if (temp != 0) {
			this->character_string = temp;
		}
		else {
			std::cout << "NULL";
		}
	}
	double character::__return__double() { // Returns the contents of a variable from double
		return atof(this->character_string.c_str());
	}
	character::character(double push_string) { // Constructor
		this->character_string = std::to_string(push_string);
	}
	bool character::empty() { // Is empty
		return this->character_string.empty();
	}
	void character::Flip() { // Reverse string order
		_strrev((char*)this->character_string.c_str());
	}
	void character::Case() { // If uppercase to lowercase if lowercase to uppercase (current variable string)
		for (int i = 0; i < (int)this->length(); i++) {
			if (this->character_string[i] >= 65 &&
				this->character_string[i] <= 90) {
				this->character_string[i] = this->character_string[i] + 32;
			}
			else if (this->character_string[i] >= 97 &&
				this->character_string[i] <= 122) {
				this->character_string[i] = this->character_string[i] - 32;
			}
		}
	}
	void character::lowercase() { // All lowercase
		for (int i = 0; i < (int)this->length(); i++) {
			if (this->character_string[i] >= 65 &&
				this->character_string[i] <= 90) {
				this->character_string[i] = this->character_string[i] + 32;
			}
		}
	}
	void character::uppercase() { // All uppercase
		for (int i = 0; i < (int)this->length(); i++) {
			if (this->character_string[i] >= 97 &&
				this->character_string[i] <= 122) {
				this->character_string[i] = this->character_string[i] - 32;
			}
		}
	}
	std::string character::GetExtension() { // Get the extension in this class string (if any)
		std::string return__;
		int start = 0;
		for (int i = 0; i < (int)this->size(); i++) {
			if (this->__Rigthcharacter__(i) == '.') {
				start = this->size() - i;
				break;
			}
		}
		return__ = this->slice(start, this->length());
		return return__;
	}
	void character::destroy() { // Delete the class 
		this->character_string.~basic_string();
	}
	void character::pop(std::string string) { // Push element
		this->character_string = string;
	}
	void character::InitializationString() { // Initialization string
		this->character_string = "@end info this character_string of the end this string @#@#@#@#@#@@";
	}
	character character::operator=(const std::wstring& str) { // Overloaded operator
		character return__;
		return__.character_string = return__.character_string + this->convert(str);
		return return__;
	}
	character character::operator+(const character& str) { // Overloaded operator
		character return__;
		return__.character_string = this->character_string + str.character_string;
		return return__;
	}
	char character::__Rigthcharacter__(int where) { // Get characters from the left
		return this->character_string[this->size() - where];
	}
	void character::Expansion(int ExpansionNum) { // Extended capacity (not recommended)
		this->character_string.resize(this->size() + ExpansionNum);
	}
	void character::__Setlength__(int num) { // Set length (same as above)
		this->character_string.resize(num);
	}
	void character::replace(std::string string, std::string str) { // Replaces what the string specifies
		if (this->ReadPermission == true) {
			this->character_string.replace(this->character_string.find(string.c_str()), this->character_string.find(string.c_str()) + string.size(), str.c_str());
		}
	}
	int character::find(int start, std::string finder) {
		int show = 0, restart = 0;
		if (this->ReadPermission == true) {
			while (true) {
				if (this->character_string.find(finder, restart) != this->character_string.npos) {
					show++;
					restart = this->character_string.find(finder, restart) + finder.size();
				}
				else {
					break;
				}
			}
		}
		return show;
	}
	void character::encryption() { // base64 encode
		std::string temp = this->base64_encode(this->character_string.c_str(), this->size() + 1);
		this->character_string = temp;
	}
	void character::Decrypt() { // base64 decode
		std::string temp = this->base64_decode(this->character_string.c_str(), this->size() + 1);
		this->character_string = temp;
	}
	unsigned int character::size() { // Get the size of the string, different from lenght (), it will return the size of the occupied bytes, level lenght () + 1
		return sizeof(this->character_string);
	}
	void character::pop() { // Pop first element
		if (this->ReadPermission == true) {
			std::string temp = this->slice(1, this->length());
			this->character_string = temp;
		}
	}
	std::string* character::GetAddress() { // Returns a reference to a string
		return &this->character_string;
	}
	std::string character::slice(int start, int over) { // String slicing
		std::string temp;
		for (int i = start; i < over; i++)
			temp = temp + this->character_string[i];
		return temp;
	}
	unsigned int character::length() { // Live length
		return this->character_string.length();
	}
	void character::get(bool Additionclear, std::string quesion) { // Enter data (cin can be used instead) if Additionclear is true append input,quesion is the out of string
		std::cout << quesion;
		if (Additionclear == true)
			std::cin >> this->character_string;
		else {
			std::string temper = this->character_string;
			std::cin >> this->character_string;
			this->character_string = temper + this->character_string;
		}
	}
	void character::get(bool Additionclear) { // Enter data(cin can be used instead) if Additionclear is true append input,
		if (Additionclear == true)
			std::cin >> this->character_string;
		else {
			std::string temper = this->character_string;
			std::cin >> this->character_string;
			this->character_string = temper + this->character_string;
		}
	}
	int character::find(std::string find_str) { // Finding strings in strings
		int temp_return = this->character_string.find(find_str);
		if (temp_return != find_str.npos)
			return temp_return;
		else
			return INT_MAX;
	}
	int character::find(std::string find_str, int Subscript) { // Finding strings in strings(starting from the specified location)
		int temp_return = this->character_string.find(find_str, Subscript);
		if (temp_return != find_str.npos)
			return temp_return;
		else
			return INT_MAX;
	}
	void character::SetReadPermissions(bool TrueOrFlase) { // Change access
		this->ReadPermission = TrueOrFlase;
	}
	character::character(int push_string) { // Constructor for user assignment
		if (this->ReadPermission == true) { // If you have permission to read
			if (this->character_string == "@end info this character_string of the end this string @#@#@#@#@#@@") // Proofreading for first use
				this->character_string = std::to_string(push_string);
			else
				this->character_string = this->character_string + std::to_string(push_string);
		}
	}
	character::character(const std::wstring& push_string) {
		this->character_string = this->convert(push_string);
	}
	character::character(char* push_string) {
		this->character_string = this->convert(push_string);
	}
	character::character(CString push_string) {
		this->character_string = this->convert(push_string);
	}
	character::character(long long push_string) {
		this->character_string = std::to_string(push_string);
	}
	character::character() {// Initialization constructor
		if (this->ReadPermission == true) { // If you have permission to read
			push_numer++;
		}
	}
	character::character(LPCSTR push_string) {
		this->character_string = this->convert(push_string);
	}
	std::string character::return_character_string() {
		if (this->ReadPermission == true) // If you have permission to read
			return this->character_string;
		else
			return "NULL";
	}
	int character::__return__int() {
		return std::atoi(this->return_character_string().c_str());
	}
	std::string character::__return__string() {
		return this->return_character_string();
	}
	std::wstring character::__return__wstring() {
		_bstr_t t = this->character_string.c_str();
		wchar_t* swap_char = (wchar_t*)t;
		std::wstring return_ = swap_char;
		return return_;
	}

	std::ostream& operator<<(std::ostream& output, const character& str) {
		output << str.character_string;
		return output;
	}
	char& character::operator[](unsigned int length) {
		if (length > this->length())
			return this->character_string[this->length() - 1];
		else
			return this->character_string[length];
	}
	bool operator!=(const character& str1, const character& str2) {
		if (str1.character_string != str2.character_string) {
			return 1;
		}
		else {
			return 0;
		}
	}
	bool operator<(const character& str1, const character& str2) {
		if (strcmp(str1.character_string.c_str(), str2.character_string.c_str()) < 0)
			return 1;
		else
			return 0;
	}
	bool operator>(const character& str1, const character& str2) {
		if (strcmp(str1.character_string.c_str(), str2.character_string.c_str()) > 0)
			return 1;
		else
			return 0;
	}
	bool operator==(const character& str1, const character& str2) {
		if (str1.character_string == str2.character_string)
			return 1;
		else
			return 0;
	}
	std::istream& operator>>(std::istream& input, character& str) {
		return input >> str.character_string;
	}
};