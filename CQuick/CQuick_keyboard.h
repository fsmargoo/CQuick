#pragma once
#include <Windows.h>
#include <vector>

namespace CQKeyboard {
	class keyboardEvent {
	private:
		char Cheak = ' ';
	public:
		keyboardEvent(char);
		void CheakUp();
		void CheakDown();
	};

	void keyboardEvent::CheakUp() {
		keybd_event(this->Cheak, KEYEVENTF_KEYUP, 0, 0);
	}
	void keyboardEvent::CheakDown() {
		keybd_event(this->Cheak, 0, 0, 0);
	}
	keyboardEvent::keyboardEvent(char Key) {
		this->Cheak = Key;
	}
}